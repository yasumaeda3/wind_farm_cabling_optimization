import numpy as np
import math
import csv
import matplotlib
from matplotlib import pyplot as plt
import os

years_in_operation = 25
power_price = 120
capacity_factor = 0.4
failure_rate = 1.07305936e-9
mttr = 1440
image_file_name = 'golfe_de_fos'
node_data_directory = './input/turbine_data/golfe_de_fos_w_ss.csv'
arc_data_directory = './input/existing_arc_data/golfe_de_fos_existing_arc.csv'


def output_data(wind_farm, arcs, image_file_name, cable_cost, cable_initial_cost, cable_loss_cost, generation_opportunity_cost):
    colormap = matplotlib.cm.get_cmap('jet')
    f, ax = plt.subplots()
    for node in wind_farm.nodes:
        plt.scatter(node.x_coordinate, node.y_coordinate, s=5)
        plt.text(node.x_coordinate, node.y_coordinate, node.id)
    for arc in arcs:
        # plt.plot([list(arc.node_frozenset)[0].x_coordinate,list(arc.node_frozenset)[1].x_coordinate],[list(arc.node_frozenset)[0].y_coordinate,list(arc.node_frozenset)[1].y_coordinate], c=str(min(0.9, -(arc.voltage/cable_max)+1)),linewidth=3)
        plt.plot([list(arc.node_frozenset)[0].x_coordinate, list(arc.node_frozenset)[1].x_coordinate], [list(arc.node_frozenset)[
                 0].y_coordinate, list(arc.node_frozenset)[1].y_coordinate], c=colormap(arc.power/105), linewidth=3)
        plt.axis('scaled')
    plt.text(0, 1.01, 'Total Cost: {cable_cost}\nInitial Cost: {cable_initial_cost}\nCable Loss Cost: {cable_loss_cost}\nOpportunity Cost: {generation_opportunity_cost}'.format(
             cable_cost='{:,}'.format(int(cable_cost)), cable_loss_cost='{:,}'.format(int(cable_loss_cost)), cable_initial_cost='{:,}'.format(int(cable_initial_cost)), generation_opportunity_cost='{:,}'.format(int(generation_opportunity_cost))), size=8, transform=ax.transAxes)
    plt.text(0.8, 1.01, 'Years in Operation:{years_in_operation}\nPower Price:{power_price}\nCapacity Factor:{capacity_factor}\nFailure Rate:{failure_rate}\nMTTR:{mttr}'.format(
        years_in_operation=years_in_operation, power_price=power_price, capacity_factor=capacity_factor, failure_rate=failure_rate, mttr=mttr), size=8, transform=ax.transAxes)
    plt.savefig("{image_file_name}.png".format(
        image_file_name=image_file_name))


class WindFarm:
    def __init__(self, nodes):
        self.nodes = sorted(nodes, key=lambda node: node.id)
        self.turbines = [
            node for node in self.nodes if node.is_turbine == True]
        self.substation = next(
            (node for node in self.nodes if node.is_substation == True), None)
        self.generate_distance_matrix()
        # self.set_list_of_sets_of_crossing_node_sets()

    def generate_distance_matrix(self):
        x_coordinates = [node.x_coordinate for node in self.nodes]
        y_coordinates = [node.y_coordinate for node in self.nodes]
        distance_matrix = []
        for i in range(len(self.nodes)):
            distance_array = []
            for j in range(len(self.nodes)):
                coordinates_1 = np.array([x_coordinates[i], y_coordinates[i]])
                coordinates_2 = np.array([x_coordinates[j], y_coordinates[j]])
                distance_array.append(
                    int(np.linalg.norm(coordinates_1 - coordinates_2)))
            distance_matrix.append(distance_array)
        self.distance_matrix = np.array(distance_matrix)


class Arc:
    def __init__(self, node_frozenset, cable_cost_per_meter, voltage, resistance, power=0):
        self.node_frozenset = node_frozenset
        self.cable_cost_per_meter = cable_cost_per_meter
        self.voltage = voltage
        self.resistance = resistance
        self.power = power


class Node:
    def __init__(self, id, x_coordinate, y_coordinate, generated_power, is_turbine=False, is_substation=False):
        self.id = id
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.generated_power = generated_power
        self.is_turbine = is_turbine
        self.is_substation = is_substation


def input_wind_farm_data(data_directory):
    nodes = []
    with open(data_directory, 'r', encoding='utf_8_sig') as file:
        csvreader = csv.reader(file)
        for row in csvreader:
            nodes.append(Node(int(row[0]), int(row[1]), int(row[2]), int(
                row[3]), bool(int(row[4])), bool(int(row[5]))))
    wind_farm = WindFarm(nodes=nodes)
    return wind_farm


def input_arc_data(data_directory, nodes):
    arcs = []
    with open(data_directory, 'r', encoding='utf-8-sig') as file:
        csvreader = csv.reader(file)
        for row in csvreader:
            node1 = next(
                (node for node in nodes if node.id == int(row[0])), None)
            node2 = next(
                (node for node in nodes if node.id == int(row[1])), None)
            arcs.append(Arc(frozenset((node1, node2)),
                        float(row[2]), float(row[3]), float(row[4]), float(row[5])))
    return arcs


def cost_analysis(node_data_directory, arc_data_directory):
    wind_farm = input_wind_farm_data(node_data_directory)

    arcs = input_arc_data(arc_data_directory, wind_farm.nodes)
    # Calculate Initial Costs
    cable_initial_cost = 0
    for arc in arcs:
        cable_initial_cost += arc.cable_cost_per_meter * \
            wind_farm.distance_matrix[list(arc.node_frozenset)[
                0].id, list(arc.node_frozenset)[1].id]

    # Calculate Power Loss Costs
    cable_loss_cost = 0
    for arc in arcs:
        cable_loss_cost += (wind_farm.distance_matrix[list(arc.node_frozenset)[0].id][list(
            arc.node_frozenset)[1].id] * arc.resistance) * ((arc.power * (10**6))/arc.voltage)**2 * capacity_factor * years_in_operation * (power_price/(10**6)) * 8760

    # Caculate Expected Value of Generation Opportunity Cost
    generation_opportunity_cost = 0
    for arc in arcs:
        # Edit this to change failure rate in accordance to power passing through said arc
        failure_rate_at_arc = failure_rate
        generation_opportunity_cost += wind_farm.distance_matrix[list(arc.node_frozenset)[0].id, list(
            arc.node_frozenset)[1].id] * failure_rate * years_in_operation * 8760 * mttr * arc.power * power_price * capacity_factor

    total_cable_cost = cable_loss_cost + cable_initial_cost

    print('total cable cost: {total_cable_cost}\ncable_loss_cost: {cable_loss_cost}\nable initial cost: {cable_initial_cost}\ngeneration opportunity cost: {generation_opportunity_cost}'.format(
        total_cable_cost='{:,}'.format(int(total_cable_cost)), cable_loss_cost='{:,}'.format(int(cable_loss_cost)), cable_initial_cost='{:,}'.format(int(cable_initial_cost)), generation_opportunity_cost='{:,}'.format(int(generation_opportunity_cost))))

    output_data(wind_farm, arcs, image_file_name, total_cable_cost,
                cable_initial_cost, cable_loss_cost, generation_opportunity_cost)


cost_analysis(node_data_directory, arc_data_directory)
